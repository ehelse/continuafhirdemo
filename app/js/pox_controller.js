/**
 * Created by LarsKristian on 14.09.2015.
 */


app.controller('POxController', function POxController($scope, $http, $routeParams, $location, Patient, Observation) {

    console.log('$routeParams:' + JSON.stringify($routeParams));
    var codes = ['150456','149530','188736','149546','150021','150022','150020'];

    $scope.message = {};

    $scope.loading = true;

    $scope.data = [];

    $scope.pulse = [];
    $scope.bpPulse = [];
    $scope.SpO2 = [];
    $scope.weight = [];
    $scope.bp = [];
    $scope.systolic = [];
    $scope.diastolic = [];

    $scope.noData = true;
    $scope.hasData = {};
    $scope.show = { pulse : true, SpO2 : true, weight: true, systolic: true, diastolic: true };

    $scope.viewPatient = function () {
        $location.path('/patient/' + $routeParams.patientId);
    };

    var findCode = function (anArray, code) {
        var arrayLength = anArray.length;
        for (var i = 0; i < arrayLength; i++) {
            var item = anArray[i];
            if (item.code === code) {
                return true;
            }
        }
        return false;
    };

    $scope.resetGraph = function() {

        $scope.data = [];
        $scope.pulse = [];
        $scope.SpO2 = [];
        $scope.weight = [];
        $scope.bp = [];
        $scope.systolic = [];
        $scope.diastolic = [];
        $scope.noData = true;
        $('#graph').empty();

        if ($scope.plot != undefined) {
            $scope.plot.destroy();
        }

        $scope.observations = Observation.get({patient: $routeParams.patientId, code: codes.join(","), '_sort:desc': 'date', _count: '500'},
            function () {
                //console.log('$scope.observations=' + JSON.stringify($scope.observations));

                if (typeof $scope.observations.entry === 'undefined') {
                    console.log('No observations');
                    $scope.loading = false;
                    console.log('Observations done');
                    $scope.message.loading = 'No measurement data for this patient.';
                    return;
                } else {
                    //console.log('$scope.observations.entry=' + $scope.observations.entry);
                }

                // Loop through observations and make graphdata
                var arrayLength = $scope.observations.entry.length;
                console.log('There are ' + arrayLength + ' observations.')
                for (var i = 0; i < arrayLength; i++) {
                    var r = $scope.observations.entry[i].resource;

                    if (typeof r.effectiveDateTime !== 'undefined') {

                        // Get time
                        var effectiveDateTime = new Date(r.effectiveDateTime);

                        console.log('effectiveDateTime = ' + effectiveDateTime);
                        //console.log('r = ' + JSON.stringify(r));
                        if (findCode(r.code.coding, "149530") && (typeof r.valueQuantity != 'undefined')) {
                            console.log('Found code 150456: ' + r.valueQuantity.value);
                            var element = [effectiveDateTime, r.valueQuantity.value, r];
                            $scope.pulse.push(element);
                            //$scope.show.pulse = true;
                            $scope.hasData.pulse = true;
                            $scope.noData = false;
                        }
                        if (findCode(r.code.coding, "150456") && (typeof r.valueQuantity != 'undefined')) {
                            console.log('Found code 149530: ' + r.valueQuantity.value);
                            var element = [effectiveDateTime, r.valueQuantity.value, r];
                            $scope.SpO2.push(element);
                            //$scope.show.SpO2 = true;
                            $scope.hasData.SpO2 = true;
                            $scope.noData = false;
                        }
                        if (findCode(r.code.coding, "188736") && (typeof r.valueQuantity != 'undefined')) {
                            console.log('Found code 149530: ' + r.valueQuantity.value);
                            var element = [effectiveDateTime, r.valueQuantity.value, r];
                            $scope.weight.push(element);
                            //$scope.show.SpO2 = true;
                            $scope.hasData.weight = true;
                            $scope.noData = false;
                        }

                        if (findCode(r.code.coding, "149546") && (typeof r.valueQuantity != 'undefined')) {
                            console.log('Found code 149546: ' + r.valueQuantity.value);
                            var element = [effectiveDateTime, r.valueQuantity.value, r];
                            $scope.bpPulse.push(element);
                            $scope.hasData.bpPulse = true;
                            $scope.noData = false;
                        }

                        // Get values
                        if (r.component === undefined || typeof r.component == undefined) {
                            //console.log('No component');
                        }
                        else {
                            var componentLength = r.component.length;
                            //console.log('Found ' + componentLength + ' components');
                            var sys = 0;
                            var dia = 0;
                            for (var ci = 0; ci < componentLength; ci++) {
                                var component = r.component[ci];
                                if (typeof component !== undefined) {
                                    console.log('Handling component:' + JSON.stringify(component));
                                    if (findCode(component.code.coding, "150021") && (typeof component.valueQuantity != 'undefined')) {
                                        console.log("Found systolic code:" + JSON.stringify(component));
                                        var element = [effectiveDateTime, component.valueQuantity.value, r];
                                        $scope.systolic.push(element);
                                        var sys = component.valueQuantity.value;
                                        $scope.hasData.diastolic = true;
                                        $scope.noData = false;
                                    } else if (findCode(component.code.coding, "150022") && (typeof component.valueQuantity != 'undefined')) {
                                        console.log("Found diastolic code:" + JSON.stringify(component));
                                        var element = [effectiveDateTime, component.valueQuantity.value, r];
                                        $scope.diastolic.push(element);
                                        var dia = component.valueQuantity.value;
                                        $scope.hasData.systolic = true;
                                        $scope.noData = false;
                                    } else {
                                        console.log('Found no code'+JSON.stringify(component.code.coding));
                                    }
                                } else {
                                    console.log('Component undefined:');
                                }
                            }
                            //[ x, y, xerr, yerr_lower, yerr_upper ]
                            var mean = (dia+sys)/2;
                            //var element = [effectiveDateTime, mean, mean-dia, -mean+sys, dia, sys];
                            //$scope.bp.push(element);
                            $scope.systolic.push[effectiveDateTime, sys];
                            $scope.diastolic.push[effectiveDateTime, dia];
                        }

                    }

                }

                //console.log('SpO2 data:' + JSON.stringify($scope.SpO2));
                //console.log('Pulse data:' + JSON.stringify($scope.pulse));

                if ($scope.hasData.SpO2 == true && $scope.show.SpO2 == true) {
                    d = { label: 'SpO2', data: $scope.SpO2};
                    $scope.data.push(d);
                }
                if ($scope.hasData.pulse == true && $scope.show.pulse == true) {
                    d = { label: 'Pulse', data: $scope.pulse};
                    $scope.data.push(d);
                }
                if ($scope.hasData.weight == true && $scope.show.weight == true) {
                    d = { label: 'Weight', data: $scope.weight};
                    $scope.data.push(d);
                }
                if ($scope.hasData.systolic == true && $scope.show.systolic == true) {
                    d = { label: 'Systolic', data: $scope.systolic};
                    $scope.data.push(d);
                }
                if ($scope.hasData.diastolic == true && $scope.show.diastolic == true) {
                    d = { label: 'Diastolic', data: $scope.diastolic};
                    $scope.data.push(d);
                }

                //console.log('hasData:'+JSON.stringify($scope.hasData));
                //console.log('show:'+JSON.stringify($scope.show));

                $scope.drawGraph();
                $scope.loading = false;
                console.log('Observations done');
            },
            function(error) {
                $scope.loading = false;
                console.log('Observations done');
                $scope.message.loading = 'No measurement data for this patient.';
            }
        );


    };


    $scope.drawGraph = function () {
        //http://atlantageek.com/2014/01/11/cheatsheet-for-flot/
        // http://www.jqueryflottutorial.com/how-to-make-jquery-flot-time-series-chart.html
        var options = {
            legend: {
                show: true
            },
            series: {
                shadowSize: 5,
                lines: {
                    show: true
                },
                points: {
                    show: true,
                    radius: 5,
                    fill: true
                }
            },
            yaxis: {
                ticks: 10,
                min: 0
            },
            xaxis: {
                mode: "time",
                timeformat: "%d/%m/%y",
                ticks: 3,
                autoscaleMargin: 0.01
            },
            selection: {
                mode: "x"
            },
            grid: {
                hoverable: true,
                clickable: true,
                backgroundColor: { colors: ["#ffffff", "#F2F2F2"] },
                labelMargin: 40
            }
        };

        $scope.plot = $.plot("#graph", $scope.data, options);

        $("#graph").bind("plotselected", function (event, ranges) {

            console.log("plotselected:" + JSON.stringify(ranges));

            // clamp the zooming to prevent eternal zoom

            if (ranges.xaxis.to - ranges.xaxis.from < 0.00001) {
                ranges.xaxis.to = ranges.xaxis.from + 0.00001;
            }

            if (ranges.yaxis.to - ranges.yaxis.from < 0.00001) {
                ranges.yaxis.to = ranges.yaxis.from + 0.00001;
            }

            // do the zooming
            options.xaxis.min = ranges.xaxis.from;
            options.xaxis.max = ranges.xaxis.to;

            $scope.plot = $.plot("#graph", $scope.data, options);

        });

        $("<div id='tooltip'></div>").css({
            position: "absolute",
            //display: "none",
            border: "1px solid #fdd",
            padding: "2px",
            "background-color": "#fee",
            opacity: 0.80
        }).appendTo("body");

        $("#graph").bind("plothover", function (event, pos, item) {

            if (item) {

                $scope.selectedResource = item.series.data[item.dataIndex][2];

                var text = item.series.label+': Value = '+$scope.selectedResource.valueQuantity.value;

                if ($scope.selectedResource.comments != undefined) {
                    text = text+' Comment: '+$scope.selectedResource.comments;
                }

                $scope.$apply(function () {
                    $scope.hoverData = text;
                });

                $("#tooltip").html(text) //item.series.label + " of " + sys + "/" + dia)
                    .css({top: item.pageY+5, left: item.pageX+5})
                    .fadeIn(200);
            } else {
                $("#tooltip").hide();
            }
        });

        $("#graph").bind("plotclick", function (event, pos, item) {
            //console.log('ITEM -> item='+JSON.stringify(item));
            //console.log('plotclick -> event:'+JSON.stringify(event));
            //console.log('plotclick -> item:'+JSON.stringify(item));
            $scope.selectedResource = item.series.data[item.dataIndex][2];

            var text = '';
            if ($scope.selectedResource.comments != undefined) {
                text = ', Comment: '+$scope.selectedResource.comments;
                $scope.plot.highlight(item.series, item.datapoint);
            }

            if (item) {
                $scope.$apply(function () {
                    $scope.clickData = "Clicked point. Value = " + $scope.selectedResource.valueQuantity.value + " in " + item.series.label+' '+text;
                });
                //$scope.plot.highlight(item.series, item.datapoint);
            }
        });

    };

    // Run resetgraph initially
    $scope.resetGraph();

    $scope.deleteSelected = function() {
        if (typeof $scope.selectedResource !== 'undefined') {
            Observation.delete({id: $scope.selectedResource.id},
                function(data) {
                    console.log('Point deleted:'+JSON.stringify(data));
                    $scope.resetGraph();
                },
                function() {
                    console.log('Point NOT deleted:'+JSON.stringify(data));
                });
        } else {
                alert('Data not selected');
        }
    };

    /******* Web socket *********/
        // https://github.com/angular-class/angular-websocket
    $scope.webSocket = new WebSocket("ws://continua.cloudapp.net/websocket/dstu2");

    $scope.webSocket.onopen = function (event) {
        console.log('onopen:'+JSON.stringify(event));
        $scope.webSocket.send('bind Observation?patient='+$routeParams.patientId);
    };

    $scope.webSocket.onmessage = function (event) {
        console.log('onmessage:'+event.data);
        if (event.data.indexOf("Observation") > -1) {
            $scope.resetGraph();
        }
    }

    $scope.webSocket.onerror = function (error) {
        console.log('websocket error:'+JSON.stringify(error));
    };
    $scope.webSocket.onclose = function (error) {
        console.log('websocket onclose:'+JSON.stringify(error));
        $scope.webSocket = new WebSocket("ws://continua.cloudapp.net/websocket/dstu2");
        console.log('Reopening websocket');
    };

});
