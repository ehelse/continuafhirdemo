console.clear(); // <-- keep the console clean on refresh

//var FHIR_URL = 'http://fire.roland.bz/base';
//var FHIR_URL = 'http://fhirtest.uhn.ca/baseDstu2';
//var FHIR_URL = 'http://continua.cloudapp.net/baseDstu2';


var settings = JSON.parse(localStorage.getItem("fhirSettings"));
if (settings === undefined || settings == null || typeof settings === 'undefined') {
    console.log('Settings not set')
    settings = { fhirUrl: FHIR_URL };
    localStorage.setItem("fhirSettings", JSON.stringify(settings));
} else {
    console.log('Settings exist:'+JSON.stringify(settings));
}

var app = angular.module('continuaDemo', ['ngRoute', 'fhirServices', 'ngResource']);


app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/', {templateUrl: 'partials/list.html', controller: 'ListController'}).
            when('/list', {templateUrl: 'partials/list.html', controller: 'ListController'}).
            when('/settings', {templateUrl: 'partials/settings.html', controller: 'SettingsController'}).
            when('/patient/:patientId', {templateUrl: 'partials/patient.html', controller: 'PatientController'}).
            when('/bp/:patientId', {templateUrl: 'partials/bp.html', controller: 'BPController'}).
            when('/pox/:patientId', {templateUrl: 'partials/pox.html', controller: 'POxController'}).
            when('/createPatient', {templateUrl: 'partials/createpatient.html', controller: 'PatientController'}).
            when('/createPatient/:patientId', {templateUrl: 'partials/createpatient.html', controller: 'PatientController'}).
            when('/metal', {templateUrl: 'partials/metal.html', controller: 'MetalController'}).
            otherwise({
                redirectTo: '/list'
            });
    }]);

app.filter('patientListFilter', function () {
    var output = [];
    return function (input) {
        //console.log('Filter input:' + JSON.stringify(input));
        //var output = [];
        if (typeof input !== 'undefined') {
            var arrayLength = input.length;
            //console.log('Array has a question array of ' + questionArrayLength + ' items.' + JSON.stringify(questionArray));
            for (var i = 0; i < arrayLength; i++) {
                if (typeof output[i] === 'undefined') {
                    var resource = {};
                    resource.id = input[i].resource.id;
                    resource.family = input[i].resource.name[0].family.toString().replace(',', ' ');
                    resource.given = input[i].resource.name[0].given.toString().replace(',', ' ');
                    resource.gender = input[i].resource.gender;
                    resource.birthDate = input[i].resource.birthDate;
                    output.push(resource);
                }
            }
        }
        //console.log('Filter output:' + JSON.stringify(output));
        return output;
    };
});

app.controller('ListController', function ListController($scope, $rootScope, $http, $routeParams, $location, Patient) {

    console.log('$routeParams:' + JSON.stringify($routeParams));
    $scope.currentPath = $location.path();
    console.log('$scope.currentPath='+$scope.currentPath);

    // FHIR connection stuff

    $scope.url = window.location.href;

    $scope.patientList = Patient.get({id: '', _count: '100'}, function () {
        //console.log('$scope.patientList='+JSON.stringify($scope.patientList));
    });

    $rootScope.$on('reloadPatientList', function (event, data) {
        console.log(data); // 'Some data'
        $scope.patientList = Patient.get({id: '', _count: '100'}, function () {
            console.log('$scope.patientList='+JSON.stringify($scope.patientList));
            window.location.reload();
        });
    });

    $scope.viewPatient = function (patientId) {
        $location.path('/patient/' + patientId);
    };

    $scope.navigate = function (patientId, page) {
        $location.path('/' + page + '/' + patientId);
    };

    $scope.clickPatientRow = function (p) {
        $location.path('/patient/' + p.id);
    };

});

app.controller('PatientController', function ListController($scope, $route, $rootScope, $http, $routeParams, $location, Patient, Observation) {

    console.log('$routeParams:' + JSON.stringify($routeParams));

    $scope.patient = Patient.get({id: $routeParams.patientId, _count: '100'}, function () {
        //console.log('$scope.patientList='+JSON.stringify($scope.patientList));
    });

    $scope.navigate = function (page) {
        $location.path('/' + page + '/' + $routeParams.patientId);
    };

    $scope.createPatient = function (id, family, given, birthDate) {
        var p = {
            "resourceType": "Patient",
            "id": id,
            "text": {
                "status": "generated",
                "div": "<div>Tekst-info om pasienten</div>"
            },
            "name": [
                {
                    "use": "official",
                    "family": [
                        family
                    ],
                    "given": [
                        given
                    ]
                }
            ],
            "birthDate": birthDate
        };
        $scope.patient = Patient.update({id: id}, p, function (status) {
            console.log('Save success:'+JSON.stringify(status));
            $location.path('/');
            $rootScope.$broadcast('reloadPatientList', {
                someProp: 'Deleted a patient!' // send whatever you want
            });
        },function (error) {
                console.log('Save failed:'+JSON.stringify(error));
                $window.alert('Save failed:'+JSON.stringify(error));
            }
        );
    };

    $scope.delete = function (page) {
        Observation.delete({patient: $routeParams.patientId},
            function () {
                $scope.patient.$delete({id: $routeParams.patientId});
                $location.path('/');
                $rootScope.$broadcast('reloadPatientList', {
                    someProp: 'Deleted a patient!' // send whatever you want
                });
            },
            function () {
                $scope.patient.$delete({id: $routeParams.patientId});
                $location.path('/');
                $rootScope.$broadcast('reloadPatientList', {
                    someProp: 'Deleted a patient!' // send whatever you want
                });
            }
        );
    };

    $scope.deleteObservation = function (code) {
        Observation.delete({patient: $routeParams.patientId, code: code},
            function () {
                window.alert('Deleted observations with code '+code+' successfully. Please reload page. ')
            },
            function () {
                window.alert('Deleted observations with code '+code+' failed. ')
            }
        );
    };

    $scope.savePatient = function(p) {
        $scope.patient.$update({id: $routeParams.patientId}, function () {
            $scope.patient = Patient.get({id: $routeParams.patientId}, function () {
                //console.log('$scope.patientList='+JSON.stringify($scope.patientList));
                $location.path('/');
                $rootScope.$broadcast('reloadPatientList', {
                    someProp: 'Edited a patient!' // send whatever you want
                });
            });
        });
    };

});

app.controller('SettingsController', function SettingsController($scope, $http, $routeParams, $location, Patient) {

    $scope.settings = JSON.parse(localStorage.getItem("fhirSettings"));
    if (typeof $scope.settings == 'undefined') {
        console.log('Settings not set')
        $scope.settings = { fhirUrl: FHIR_URL };
    } else {
        console.log('Settings:'+JSON.stringify($scope.settings));
    }

    $scope.saveSettings = function () {
        console.log('Saving settings:'+JSON.stringify($scope.settings));
        localStorage.setItem("fhirSettings", JSON.stringify($scope.settings));
    };
});

