/**
 * Created by LarsKristian on 14.09.2015.
 */


app.controller('BPController', function BPController($scope, $http, $routeParams, $location, Patient, Observation) {

    console.log('$routeParams:' + JSON.stringify($routeParams));
    var codes = ['55284-4'];

    $scope.systolic = [];
    $scope.diastolic = [];
    $scope.bp = [];

    var findCode = function (anArray, code) {
        var arrayLength = anArray.length;
        for (var i = 0; i < arrayLength; i++) {
            var item = anArray[i];
            if (item.code === code) {
                return true;
            }
        }
        return false;
    };

    $scope.data = Observation.get({patient: $routeParams.patientId, code: codes.join(",")}, function () {
        console.log('$scope.data='+JSON.stringify($scope.data));

        if (typeof $scope.data.entry === 'undefined') {
            console.log('No data');
            return;
        } else {
            console.log('$scope.data.entry='+$scope.data.entry);
        }

        // Loop through data and make graphdata
        var arrayLength = $scope.data.entry.length;
        console.log('There are ' + arrayLength + ' observations.')
        for (var i = 0; i < arrayLength; i++) {
            var r = $scope.data.entry[i].resource;

            // Get time
            var effectiveDateTime = new Date(r.effectiveDateTime);
            console.log('effectiveDateTime = ' + effectiveDateTime);

            // Get values
            if (typeof r.component !== undefined) {
                var componentLength = r.component.length;
                console.log('Found ' + componentLength + ' components');
                var sys = 0;
                var dia = 0;
                for (var ci = 0; ci < componentLength; ci++) {
                    var component = r.component[ci];
                    if (typeof component !== undefined) {
                        console.log('Handling component:' + JSON.stringify(component));
                        if (findCode(component.code.coding, "8480-6")) {
                            console.log("Found systolic code:" + JSON.stringify(component));
                            var element = [effectiveDateTime, component.valueQuantity.value];
                            $scope.systolic.push(element);
                            var sys = component.valueQuantity.value;
                        } else if (findCode(component.code.coding, "8462-4")) {
                            console.log("Found diastolic code:" + JSON.stringify(component));
                            var element = [effectiveDateTime, component.valueQuantity.value];
                            $scope.diastolic.push(element);
                            var dia = component.valueQuantity.value;
                        } else {
                            console.log('Found no code');
                        }
                    } else {
                        console.log('Component undefined:');
                    }
                }
                //[ x, y, xerr, yerr_lower, yerr_upper ]
                var mean = (dia+sys)/2;
                var element = [effectiveDateTime, mean, mean-dia, -mean+sys, dia, sys];
                $scope.bp.push(element);
            }
        }

        //console.log('Systolic data:'+JSON.stringify($scope.systolic));
        //console.log('Systolic data:'+JSON.stringify($scope.diastolic));
        console.log('BP data:' + JSON.stringify($scope.bp));
        $scope.drawGraph();

        //$scope.$apply(function () {
        //    $scope.systolic.push(element);
        //});

    });

    $scope.drawGraph = function () {
        var options = {
            legend: {
                show: true
            },
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true,
                    errorbars: "y",
                    xerr: {
                        show: false,
                        asymmetric: true,
                        upperCap: "-",
                        lowerCap: "-",
                        radius: 5
                    },
                    yerr: {
                        show: true,
                        asymmetric: true,
                        upperCap: "-",
                        lowerCap: "-",
                        radius: 5
                    }
                }
            },
            yaxis: {
                ticks: 10,
                max: 200,
                min: 0
            },
            xaxis: {
                mode: "time",
                timeformat: "%d/%m/%y",
                ticks: 3
            },
            selection: {
                mode: "x"
            },
            grid: {
                hoverable: true,
                clickable: true
            }
        };

        var data = [
            /*{
             label: "Systolic",
             data: $scope.systolic
             },
             {
             label: "Diastolic",
             data: $scope.diastolic
             },*/
            {
                label: "Blood pressure",
                data: $scope.bp,
                color: "blue"
            }];

        var plot = $.plot("#graph", data, options);

        $("#graph").bind("plotselected", function (event, ranges) {

            console.log("plotselected:" + JSON.stringify(ranges));

            // clamp the zooming to prevent eternal zoom

            if (ranges.xaxis.to - ranges.xaxis.from < 0.00001) {
                ranges.xaxis.to = ranges.xaxis.from + 0.00001;
            }

            if (ranges.yaxis.to - ranges.yaxis.from < 0.00001) {
                ranges.yaxis.to = ranges.yaxis.from + 0.00001;
            }

            // do the zooming
            options.xaxis.min = ranges.xaxis.from;
            options.xaxis.max = ranges.xaxis.to;

            var plot = $.plot("#graph", data, options);

        });

        $("<div id='tooltip'></div>").css({
            position: "absolute",
            display: "none",
            border: "1px solid #fdd",
            padding: "2px",
            "background-color": "#fee",
            opacity: 0.80
        }).appendTo("body");

        $("#graph").bind("plothover", function (event, pos, item) {

            if ($("#enablePosition:checked").length > 0) {
                var str = "(" + pos.x.toFixed(2) + ", " + pos.y.toFixed(2) + ")";
                $("#hoverdata").text(str);
            }


            if (item) {
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2),
                    e1 = item.datapoint[2].toFixed(2);

                var sys = Number(y)+Number(e1);
                var dia = Number(y)-Number(e1);

                console.log('Item:'+JSON.stringify(item));

                $("#tooltip").html(item.series.label + " of " + sys + "/" + dia)
                    .css({top: item.pageY+5, left: item.pageX+5})
                    .fadeIn(200);
            } else {
                $("#tooltip").hide();
            }
        });

        $("#graph").bind("plotclick", function (event, pos, item) {
            if (item) {
                $("#clickdata").text("Clicked point " + item.dataIndex + " in " + item.series.label);
                plot.highlight(item.series, item.datapoint);
            }
        });

    };

});