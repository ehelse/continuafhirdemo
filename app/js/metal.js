/**
 * Created by LarsKristian on 13.09.2015.
 */

app.controller('MetalController', function MetalController($scope, $http, $routeParams, $location, Patient) {

    $scope.settings = {};
    $scope.settings.fhirUrl = FHIR_URL;

    $scope.sendToServer = function(method, url, json) {
        $.ajax({
            type: method,
            url: url,
            headers: {
                "Authorization":"Bearer token"
            },
            data: JSON.stringify(json),
            success: function (data) {
                $scope.$apply(function () {
                    $scope.message = 'Data sent';
                });
            },
            datatype: 'json',
            contentType: 'application/json+fhir;charset=utf-8'
        }).fail(function(error) {
            console.log('Save error:'+error+','+JSON.stringify(error));
            $scope.returnValue = JSON.stringify(error);
        }).done(function( data ) {
            console.log( "Sample of data:"+JSON.stringify(data));
            $scope.returnValue = JSON.stringify(data);
        });
    };

});